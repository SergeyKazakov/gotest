package news

import(
	"goTest/data"
	"goTest/util"
	"github.com/gin-gonic/gin"
	"strconv"
	"errors"
	"log"
)

//piece of news
type Piece struct{
	Id         int    `db:"Id"           json:"Id"`
	Title      string `db:"Title"        json:"Title"`
	Content    string `db:"Content"      json:"Content"`
	Categories []int  `json:"Categories"`
}

func GetNews(c *gin.Context) {
	news := []Piece{}
	err := data.DB.Select(&news, "SELECT * FROM News")

	if err != nil {
		util.ReportError(c, 400, err)
		return
	}

	for idx, piece := range news {
		news[idx].Categories = []int{}

		err := data.DB.Select(&news[idx].Categories, "SELECT `CategoryId` FROM `NewsCategories`, `News` WHERE `NewsId`=`Id` AND `Id`=?", piece.Id)

		if err != nil{
			util.ReportError(c, 500, err)
			return
		}

		log.Print(news[idx].Title)
	}

	util.RespUnescapedJSON(c, 200, news)
}


func EditPiece(c *gin.Context){
	IdStr := c.Param("Id")

	Id, err := strconv.Atoi(IdStr)
	if err != nil {
		util.ReportError(c, 400, errors.New("Parameter \"Id\" must be integer"))
		return
	}

	if !pieceExists(Id){
		util.ReportError(c, 400, errors.New("This piece of news does not exist"))
		return
	}

	var piece Piece
	err = c.BindJSON(&piece)
	if err != nil {
		util.ReportError(c, 400, err)
		return
	}

	if piece.Title == "" || piece.Content == "" || piece.Categories == nil {
		util.ReportError(c, 400, errors.New("None of the fields can be empty"))
		return
	}

	
	//Create transaction
	tx, err := data.DB.Begin()
	if err != nil {
		util.ReportError(c, 500, err)
		return
	}
	
	_, err = tx.Exec("UPDATE `News` SET `Title`=?, `Content`=? WHERE `Id`=?", piece.Title, piece.Content, Id)
	if err != nil {
		util.ReportErrorAndRollback(c, 500, err, tx)
		return
	}

	_, err = tx.Exec("DELETE FROM `NewsCategories` WHERE `NewsId`=?", Id)
	if err != nil {
		util.ReportErrorAndRollback(c, 500, err, tx)
		return
	}

	for _, category := range piece.Categories {
		_, err = tx.Exec("INSERT INTO `NewsCategories` (`NewsId`, `CategoryId`) VALUES (?, ?)", Id, category)
		if err != nil {
			util.ReportErrorAndRollback(c, 500, err, tx)
			return
		}	
	}

	err = tx.Commit()
	if err != nil {
		util.ReportError(c, 500, err)
		return
	}
}

func pieceExists(Id int) bool {
	entries := []int{}
	err := data.DB.Select(&entries, "SELECT `Id` FROM `News` WHERE `Id`=?", Id)

	if err != nil {
		log.Print(err)
		return false
	}

	return len(entries) == 1
}