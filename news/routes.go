package news

import(
	"github.com/gin-gonic/gin"
)

func Register(router *gin.Engine) {
	router.GET("/list", GetNews)

	router.POST("/edit/:Id", EditPiece)
}