package util

import(
	"github.com/gin-gonic/gin"
	"database/sql"
	"net/http"
	"encoding/json"
	"log"
)

func ReportError(c *gin.Context, respCode int, err error){
	RespUnescapedJSON(c, respCode, gin.H{
		"error": err.Error(),
	})
}

func ReportErrorAndRollback(c *gin.Context, respCode int, err error, tx *sql.Tx){
	RespUnescapedJSON(c, respCode, gin.H{
		"error": err.Error(),
	})

	tx.Rollback()
}


// copied from render.go
func writeContentType(w http.ResponseWriter, value []string) {
    header := w.Header()
    if val := header["Content-Type"]; len(val) == 0 {
        header["Content-Type"] = value
    }
}


func RespUnescapedJSON(c *gin.Context, code int, obj interface{}) {
    c.Status(code)
    writeContentType(c.Writer, []string{"application/json; charset=utf-8"})
    enc := json.NewEncoder(c.Writer)
    enc.SetEscapeHTML(false) // this is missing in gin
    if err := enc.Encode(obj); err != nil {
        log.Fatal(err)
    }
}