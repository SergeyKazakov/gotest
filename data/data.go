package data

import(
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"os"
	"log"
	"fmt"
)

var DB *sqlx.DB

func retrieveEnv(name string) string {
	content := os.Getenv(name)
	if content == "" {
		log.Fatalln("Variable is not set:", name)
	}

	return content
}

func init() {
	log.Println("Initializing database")

	user     := retrieveEnv("GOTEST_USERNAME")
	password := retrieveEnv("GOTEST_PASSWORD")
	dbName   := retrieveEnv("GOTEST_DB_NAME")

	source := fmt.Sprintf("%s:%s@/%s", user, password, dbName)

	var err error
	DB, err = sqlx.Connect("mysql", source)

	if err != nil {
		log.Fatalln(err)
	}
}