package main

import(
	"github.com/gin-gonic/gin"
	"goTest/news"
)



func main(){
	router := gin.Default()

	news.Register(router)

	router.Run()
}